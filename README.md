# referee-management-tool

This application allows the allocation of referees to matches. It will most likely be hosted on Azure, and has CI on via Travis

[![Build Status](https://dev.azure.com/raresmateizer/referee-management-tool/_apis/build/status/2?branchName=master)](https://dev.azure.com/raresmateizer/referee-management-tool/_build/latest?definitionId=2&branchName=master)

[![Build Status](https://travis-ci.org/rares985/referee-management-tool.svg?branch=develop)](https://travis-ci.org/rares985/referee-management-tool)
